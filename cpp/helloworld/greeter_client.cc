/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include <iostream>
#include <memory>
#include <string>
#include <fstream>
#include <chrono>
#include <thread>
#include <unistd.h>
#include <csignal>

#include <grpcpp/grpcpp.h>

#include "helloworld.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;
using helloworld::HelloRequest;
using helloworld::HelloReply;
using helloworld::Greeter;

bool stop = false;

class GreeterClient {
 public:
  GreeterClient(std::shared_ptr<Channel> channel)
      : stub_(Greeter::NewStub(channel)) {}

  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  std::string SayHello(const std::string& user) {
    // Data we are sending to the server.
    HelloRequest request;
    request.set_name(user);

    // Container for the data we expect from the server.
    HelloReply reply;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->SayHello(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return reply.message();
    } else {
      return "RPC failed";
    }
  }

 private:
  std::unique_ptr<Greeter::Stub> stub_;
};


void worker_thread(int id, int iter, std::string server) {
    GreeterClient greeter(
        grpc::CreateChannel(server, grpc::InsecureChannelCredentials()));

    std::vector<std::pair<int64_t, double>> delay;
    while (!stop) {
      auto ts = std::chrono::system_clock::now();
        
      auto t1 = std::chrono::steady_clock::now();
      std::string user(sizeof(int64_t), 'a');
      std::string reply = greeter.SayHello(user);
      if ("RPC failed" == reply) continue;
      auto t2 = std::chrono::steady_clock::now();
      std::chrono::duration<double> elapsed_time = t2 - t1;
      // elapsed_time is in second
      delay.emplace_back(ts.time_since_epoch().count(), elapsed_time.count());
    }
    std::ofstream fs;
    char hostname[1024];
    gethostname(hostname, 1023);
    std::string filename = hostname + std::string("_") + std::to_string(id) + "_latency";
    fs.open(filename);
    for (const auto l : delay) {
        fs << l.first << ", " << l.second << std::endl;
    }
    fs.close();
    
}

void SingalHandler(int s) {
    stop = true;
}

int main(int argc, char** argv) {
    int num_iter = 10;
    std::string target_str;
    std::vector<std::thread> thread_pool;
    std::string server("s0:50051");
    int num_thread = atoi(argv[1]);

    std::signal(SIGTERM, SingalHandler);
    std::signal(SIGINT, SingalHandler);

    for(int i = 0; i < num_thread; ++i) {
        thread_pool.emplace_back(worker_thread, i, num_iter, server);
    }

    for(auto& t : thread_pool) {
        t.join();
    } 
    std::cout<<"Done"<<std::endl;
    return 0;
}
